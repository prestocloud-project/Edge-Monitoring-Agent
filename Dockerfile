FROM openjdk:8-jre

MAINTAINER "Salman Taherizadeh - Jozef Stefan Institute"

##############################################################
RUN export TERM=xterm
RUN mkdir -p "/JavaFiles"
WORKDIR /JavaFiles
#### wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
#### unzip java-json.jar.zip
COPY java-json.jar /JavaFiles/java-json.jar
#### wget https://www.dropbox.com/s/7vwoheznnywe289/mqtt-producer-6.0-jar-with-dependencies.jar
COPY mqtt-producer-6.0-jar-with-dependencies.jar /JavaFiles/mqtt-producer-6.0-jar-with-dependencies.jar
COPY Send.class /JavaFiles/Send.class
COPY start.sh /JavaFiles/start.sh
RUN chmod 777 /JavaFiles/start.sh
COPY consul /opt/consul
RUN chmod 777 /opt/consul
RUN mkdir -p "/etc/consul.d"
###############
ENV CLASSPATH=.:/JavaFiles/java-json.jar:/JavaFiles/mqtt-producer-6.0-jar-with-dependencies.jar
##############################################################

ENTRYPOINT ["/JavaFiles/start.sh"]
