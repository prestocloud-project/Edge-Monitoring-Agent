#!/bin/bash

exec java Send $Broker_IP $Device_Id $Device_Type $Interval_Sec $Group_Id &

bind_ip=`awk 'END{print $1}' /etc/hosts`

/opt/consul agent -join=$ConsulServer_IP -data-dir=/tmp/consul -bind=$bind_ip -advertise=$Host_IP -enable-script-checks=true -client=0.0.0.0 -config-dir=/etc/consul.d



