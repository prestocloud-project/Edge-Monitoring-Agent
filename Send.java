import producer.MqttProducer;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.IOException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.HashMap;
import java.util.Map.Entry;

import java.text.DecimalFormat;

class Send {

	static final String PATHCPU = "/proc/stat";
	static final String PATHMEM = "/proc/meminfo";
	static double cpu_user = 0.0;
	static double cpu_sys = 0.0;
	static double cpu_io_wait = 0.0;
	static int mem_free = 0;
	static HashMap<String,Long> lastValuesCPU;
  
	public static void main(String argv[]) throws Exception {
		try{
			String Broker_IP = argv [0];
			String Device_Id = argv [1];
			String Device_Type = argv [2];
			String Interval_Sec = argv [3];
			String Group_Id = argv [4];
			String topic = "monitoring" + "/" + Group_Id + "/" + Device_Type + "/" + Device_Id;
			//MemoryPersistence persistence = new MemoryPersistence();
			//MqttClient sc = new MqttClient("tcp://" + Broker_IP + ":1883", "", persistence);
			MqttProducer sc = new MqttProducer(Broker_IP, topic);
			//MqttConnectOptions connOpts = new MqttConnectOptions();
			//connOpts.setCleanSession(true);
			//sc.connect(connOpts);
			lastValuesCPU = getValuesCPU();
			while(true){
				long lasttime = System.currentTimeMillis();
				/////////////////////////////
				/////////////////////////////////////////////////////////
				long unixTime = System.currentTimeMillis() / 1000L;
				Process P;
				BufferedReader StdInput;
				String resultCom;
				JSONObject jsonObject;
				///////////////////
				jsonObject = new JSONObject();
				jsonObject.put("timestamp", unixTime);
				///////////////////
				///////CPU
				measureValuesCPU();
				DecimalFormat numberFormat = new DecimalFormat("0.00");
				jsonObject.put("cpu_user", Double.parseDouble(numberFormat.format(cpu_user)));
				jsonObject.put("cpu_sys", Double.parseDouble(numberFormat.format(cpu_sys)));
				jsonObject.put("cpu_io_wait", Double.parseDouble(numberFormat.format(cpu_io_wait)));
				///////mem_free
				measureValuesMem();
				jsonObject.put("mem_free", mem_free);
				///////disk_available
				String[] disk_available={"/bin/bash","-c","df -k /tmp | tail -1 | awk \'{print $4}\'"};
				P = Runtime.getRuntime().exec(disk_available);
				P.waitFor();
				StdInput = new BufferedReader(new InputStreamReader(P.getInputStream()));
				resultCom= StdInput.readLine();
				double tmp_disk = Double.parseDouble(resultCom);
				tmp_disk = tmp_disk/1024;
				tmp_disk = tmp_disk/1024;
				jsonObject.put("disk_available", Double.parseDouble(numberFormat.format(tmp_disk)));
				/////////////////////////////////////////////////////////
				//MqttMessage message = new MqttMessage(jsonObject.toString().getBytes());
				sc.publish(jsonObject.toString());
				/////////////////////////////////////////////////////////
				//System.out.println(jsonObject.toString());
				/////////////////////////////////////////////////////////
				//channel.close();
				//connection.close();
				///////////////////////////
				long curtime = System.currentTimeMillis();
				Thread.sleep(Math.abs((1000*Integer.parseInt(Interval_Sec)) - (curtime-lasttime)));
			}
		} catch (InterruptedException | IOException e1) {e1.printStackTrace();}
	}
	
	public static HashMap<String,Long> getValuesCPU(){
		HashMap<String,Long> stats = new HashMap<String,Long>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(PATHCPU)));
			String line;
			while((line = br.readLine())!=null){
				String[] tokenz = null;
				if (line.startsWith("cpu ")){
					tokenz = line.split("\\W+");
					stats.put("cpuUser", Long.parseLong(tokenz[1]));
					stats.put("cpuNice", Long.parseLong(tokenz[2]));
					stats.put("cpuSystem", Long.parseLong(tokenz[3]));
					stats.put("cpuIdle", Long.parseLong(tokenz[4]));
					stats.put("cpuIOWait", Long.parseLong(tokenz[5]));
					stats.put("cpuIrq", Long.parseLong(tokenz[6]));
					stats.put("cpuSoftIrq", Long.parseLong(tokenz[7]));
					if(tokenz.length>8)
						stats.put("cpuSteal", Long.parseLong(tokenz[8]));
					else stats.put("cpuSteal", 0L);
					break;
				}	
			}
	        br.close();
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    long TotalCPU = 0;
	    for (Long value : stats.values())
	        TotalCPU += value;
	    stats.put("TotalCPU", TotalCPU);	
		return stats;
	}
	
	public static void measureValuesCPU() {
		HashMap<String,Long> curValuesCPU = getValuesCPU();
		HashMap<String,Long> diffValuesCPU = new HashMap<String,Long>();
		for (Entry<String,Long> entry : lastValuesCPU.entrySet()) {
	        String key = entry.getKey();
	        Long val = entry.getValue();
			diffValuesCPU.put(key, (Long)curValuesCPU.get(key) - val);
	    }
		double CPU_Total;
	    double CPU_User;
	    double CPU_Nice;
	    double CPU_System;
	    double CPU_Idle;
	    double CPU_IOwait;
	    double TotalCPU = 1.0 * diffValuesCPU.get("TotalCPU");
		//System.out.println("TotalCPU: " + TotalCPU); //////////////////////////////////////////////
	    if(TotalCPU != 0){
		    CPU_User = diffValuesCPU.get("cpuUser") / TotalCPU * 100;
	        CPU_Nice = diffValuesCPU.get("cpuNice") / TotalCPU * 100;
	        CPU_System = diffValuesCPU.get("cpuSystem") / TotalCPU * 100;
	        CPU_Total = CPU_User + CPU_Nice + CPU_System;
	        CPU_Idle = diffValuesCPU.get("cpuIdle") / TotalCPU * 100;
	        CPU_IOwait = diffValuesCPU.get("cpuIOWait") /TotalCPU * 100;
	    }
	    else{
	       	CPU_Total = 0.0;
	 	    CPU_User = 0.0;
	 	    CPU_Nice = 0.0;
	 	    CPU_System = 0.0;
	 	    CPU_Idle = 0.0;
	 	    CPU_IOwait = 0.0;
	    }
		cpu_user = CPU_User;
		cpu_sys = CPU_System;
		cpu_io_wait = CPU_IOwait;
	    lastValuesCPU = curValuesCPU;
	}
	
	public static void measureValuesMem(){
		HashMap<String,Long> stats = new HashMap<String,Long>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(PATHMEM)));
			String line;
			while((line = br.readLine())!=null){
				if (line.startsWith("MemFree")){
					mem_free = Integer.parseInt((line.split("\\W+")[1]));
					break;
				}
			}
	        br.close();
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}