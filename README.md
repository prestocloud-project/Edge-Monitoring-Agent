# Edge Monitoring Agent

## Definition
Edge Monitoring Agent is a component running on an edge node (e.g. Raspberry Pi) which is able to measure different infrastructure-level metrics (such as CPU, memory, disk, and so on).

## Edge Monitoring Agent Installation
When the Edge Monitoring Agent container is instantiated on an edge node (e.g. Raspberry Pi), it firstly registers itself into the Consul Server, and then starts sending the monitoring data to the broker. The following command should be used to instantiate the Edge Monitoring Agent container:
<br /><br />`sudo docker run -e Broker_IP="YYYYY" -e Device_Type="YYYYY" -e Device_Id="YYYYY" -e Group_Id="YYYYY" -e Interval_Sec="YYYYY" -e ConsulServer_IP="YYYYY" -e Host_IP="YYYYY" salmant/prestocloud_jsi_nis_ubi_monitoring_agent_edge`
<br /><br />Example:
<br />`sudo docker run -e Broker_IP="212.101.173.10" -e Device_Type="rpi3edge" -e Device_Id="edge4" -e Group_Id="city-yt" -e Interval_Sec="10" -e ConsulServer_IP="212.101.173.2" -e Host_IP="95.87.154.150" salmant/prestocloud_jsi_nis_ubi_monitoring_agent_edge`
<br /><br />In the above code, it should be noted that:
* `Broker_IP` is the IP address of the broker.
* `Interval_Sec` is the monitoring interval in second.
* `ConsulServer_IP` is the IP address of the consul server.
* `Host_IP` is the IP address of the edge node itself.
* `Device_Type`, `Device_Id` and `Group_Id` are explained in the deliverable D6.1 (Architecture of the PrEstoCloud platform).


